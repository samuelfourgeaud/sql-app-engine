const express = require("express");
const app = express();
const port = process.env.PORT || 8080;
const mysql = require("promise-mysql");

const request = "SELECT * FROM testtable";
const requestInsert = "INSERT INTO testtable values(888)";
const createTable = "CREATE TABLE testtable (col int)";

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

app.get("/", async (req, res) => {
  res.send("hello");
});

app.get("/secret", async (req, res) => {
  const secret = (await getSecret()) || "Nope";
  const project = process.env.GOOGLE_CLOUD_PROJECT;
  res.send(secret + "---" + project);
});

app.get("/sql", async (req, res) => {
  console.log("called /sql");
  const pool = await createPool();
  console.log("1===============");
  console.log(pool);
  console.log("2===============");
  const result = await pool.query(request);
  console.log("3===============");
  res.send(result);
});

app.get("/ip", async (req, res) => {
  console.log("called /ip");
  console.log(req.headers["x-appengine-user-ip"]);
  console.log(req.headers["x-forwarded-for"]);
  console.log(req.headers["forwarded"]);
  const localIp = await getIp();
  res.send(
    `x-appengine-user-ip: ${req.headers["x-appengine-user-ip"]} <br> x-forwarded-for: ${req.headers["x-forwarded-for"]} <br> forwarded: ${req.headers["forwarded"]} <br> Local IP: ${localIp}`
  );
});

app.get("/headers", async (req, res) => {
  console.log("called /ip");
  console.log(req.headers);
  res.send(`headers: ${JSON.stringify(req.headers, null, "</br>")}`);
});

app.get("/sqlinsert", async (req, res) => {
  console.log("called /sqlinsert");
  const pool = await createPool();
  console.log("1===============");
  console.log(pool);
  console.log("2===============");
  const result = await pool.query(requestInsert);
  console.log("3===============");
  res.send(result);
});

app.get("/tablecreate", async (req, res) => {
  console.log("called /tablecreate");
  const pool = await createPool();
  console.log("1===============");
  console.log(pool);
  console.log("2===============");
  const result = await pool.query(createTable);
  console.log("3===============");
  res.send(result);
});

const createPool = async () => {
  console.log("create Pool");
  const config = {
    // [START cloud_sql_mysql_mysql_limit]
    // 'connectionLimit' is the maximum number of connections the pool is allowed
    // to keep at once.
    connectionLimit: 5,
    // [END cloud_sql_mysql_mysql_limit]

    // [START cloud_sql_mysql_mysql_timeout]
    // 'connectTimeout' is the maximum number of milliseconds before a timeout
    // occurs during the initial connection to the database.
    connectTimeout: 10000, // 10 seconds
    // 'acquireTimeout' is the maximum number of milliseconds to wait when
    // checking out a connection from the pool before a timeout error occurs.
    acquireTimeout: 10000, // 10 seconds
    // 'waitForConnections' determines the pool's action when no connections are
    // free. If true, the request will queued and a connection will be presented
    // when ready. If false, the pool will call back with an error.
    waitForConnections: true, // Default: true
    // 'queueLimit' is the maximum number of requests for connections the pool
    // will queue at once before returning an error. If 0, there is no limit.
    queueLimit: 0, // Default: 0
    // [END cloud_sql_mysql_mysql_timeout]

    // [START cloud_sql_mysql_mysql_backoff]
    // The mysql module automatically uses exponential delays between failed
    // connection attempts.
    // [END cloud_sql_mysql_mysql_backoff]
  };

  return createUnixSocketPool(config);
};

const createUnixSocketPool = async config => {
  console.log("createUnixSocketPool");
  const dbSocketPath = process.env.DB_SOCKET_PATH || "/cloudsql";

  // Establish a connection to the database
  return mysql.createPool({
    user: "root", // e.g. 'my-db-user'
    password: "sam", // e.g. 'my-db-password'
    database: "test", // e.g. 'my-database'
    // If connecting via unix domain socket, specify the path
    socketPath: `${dbSocketPath}/aqueous-depth-345618:northamerica-northeast1:northamerica-replica`,
    // Specify additional properties here.
    ...config,
  });
};

// Import the Secret Manager client and instantiate it:
const { SecretManagerServiceClient } = require("@google-cloud/secret-manager");
const client = new SecretManagerServiceClient();

async function getSecret() {
  // Access the secret.
  const [accessResponse] = await client.accessSecretVersion({
    name: "projects/fluent-edition-344820/secrets/app-engine-secret/versions/latest",
  });

  const responsePayload = accessResponse.payload.data.toString("utf8");
  console.info(`Payload: ${responsePayload}`);
  return responsePayload;
}

async function getIp() {
  const fetch = require("node-fetch");
  const response = await fetch("http://api.ipify.org");
  const json = await response.text();
  return json;
}
